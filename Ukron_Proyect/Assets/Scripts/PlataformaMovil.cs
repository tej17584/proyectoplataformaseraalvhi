﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlataformaMovil : MonoBehaviour
{
    public Transform target;
    public float SpeedPlattform;

    private Vector3 start, end; //posicion inicial y final de la plataforma(donde esta el target)

    // Use this for initialization
    void Start()
    {
        // if que chequea si target no es null para que el target hijo de plataformaMovil se vuelva independiente
        if (target != null)
        {
            target.parent = null; //de esta manera pasa a ser parte de la raiz principal en hierarchy view para que plataformaMovil lo persiga
            start = transform.position;
            end = target.position;
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void FixedUpdate()
    {
        if (target != null)
        {
            float fixedSpeed = SpeedPlattform * Time.deltaTime;
            transform.position = Vector3.MoveTowards(transform.position, target.position, fixedSpeed);
        }
        if (transform.position == target.position)
        {
            //cuando la posicion de la plataforma sea la del target
            //por lo tanto si estamos al principio el target estara al final
            target.position = (target.position == start) ? end : start;
        }
    }
}
