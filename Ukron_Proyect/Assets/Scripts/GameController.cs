﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

    public bool gameover = false; //variable para terminar el juego
    public int Score = 0; //el punteo
    public float timeS = 0;
    public float timeM = 0;

    public int extrahealth = 0;

    public Text textScore;
    public Text textTime;
    public Text ExtraHealth;


    public static GameController instance; //variable que sirve para poder accesar a variables Ej. Score

    // Use this for initialization
    void Start () {
        instance = this;
    }

    // Update is called once per frame
    void Update() {
        textTime.text = "Time: 0" + timeM.ToString("n0") + ":" + timeS.ToString("n0");

        textScore.text = "Score: " + Score.ToString("n0"); // n0 sirve para no mostrar decimales
        HighScore();

        ExtraHealth.text = extrahealth.ToString(); ;

        
        if (timeS < 60)
        {
            timeS = timeS + Time.deltaTime;
        }
        else {
            timeS = 0;
            timeM = 1;
        }
    }

    public void ChangeScene(string scenename)
    {
        SceneManager.LoadScene(scenename);
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void HighScore()
    {
        if (gameover == true)
        {
            if (PlayerPrefs.GetFloat("HighScore") < Score)
            { //se toma el valor de highscore y se compara con timeScore
                PlayerPrefs.SetFloat("HighScore", Score); //se actualiza el valor de HighScore
            }

        }
    }
}
