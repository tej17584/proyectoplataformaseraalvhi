﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckGround : MonoBehaviour {


    private Player player; //referencia del jugador o personaje
    //private Rigidbody2D rb; //referencia 
    
    // Use this for initialization
    void Start () {
        player = GetComponentInParent<Player>();
        //rb = GetComponentInParent<Rigidbody2D>();
    }

    private void OnCollisionStay2D(Collision2D collision){
        if (collision.gameObject.tag == "Ground")
        {
            player.grounded = true;
        }
        if (collision.gameObject.tag == "Plattform")
        {
            //cuando se colisione con una plataforma la colision se volvera el componente padre del jugador de manera 
            //que el jugador se mueva junto con la plataforma movil
            player.transform.parent = collision.transform;
            player.grounded = true;
        }
    }

    /**
     * Metodo que detecta cuando se termina una colision
     */
    void OnCollisionExit2D(Collision2D collision){
        if (collision.gameObject.tag == "Ground"){
            player.grounded = false;
        }
        if (collision.gameObject.tag == "Plattform")
        {
            player.transform.parent = null; //al dejar de tocar la plataforma el componente padre del jugador se vuelve null 
            player.grounded = false;
        }
    }
        
        
}
