﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{

    public GameObject follow;

    public Vector2 minCamPosition, maxCamPosition; //variables que guian el movimiento de la camara de una minima a una maxima posicion 

    public float smoothTime; //tiempo de retardo

    private Vector2 velocity;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    // se ejecuta el mismo numero de veces que el framrate del juego
    private void FixedUpdate()
    {
        //float posX = follow.transform.position.x; //varible para seguimiento de la camara
        //float posY = follow.transform.position.y;

        //se suavisa el movimiento de la camara desde la transform.position.x hasta follow.transform.position.x, 
        //se gestiona usando velocity y en el periodo de timpo smoothTime
        float posX = Mathf.SmoothDamp(transform.position.x, follow.transform.position.x, ref velocity.x, smoothTime);
        float posY = Mathf.SmoothDamp(transform.position.y, follow.transform.position.y, ref velocity.y, smoothTime);

        // se colocan los rangos de la posicion en X y en Y
        transform.position = new Vector3(
            Mathf.Clamp(posX, minCamPosition.x, maxCamPosition.x),
            Mathf.Clamp(posY, minCamPosition.y, maxCamPosition.y),
            transform.position.z);
    }
}
