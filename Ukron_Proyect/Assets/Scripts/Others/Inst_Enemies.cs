﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Esta clase instancia a nuevos enimigos a lo largo del nivel
 */
public class Inst_Enemies : MonoBehaviour {

    public GameObject enemy;

    public Player player;
    //private Rigidbody2D rb;

    public float EnemyminXPosition = 0; //variables para indicar el rango del randomX
    public float EnemymaxXPosition = 0;

    public float ExtraPosY = 0;

    private float Xposition;
    private float Yposition;


    // Use this for initialization
    void Start () {
        //rb = GetComponent<Rigidbody2D>();
        
    }
	
	// Update is called once per frame
	void Update () {
        Xposition = player.transform.position.x;
        Yposition = player.transform.position.y;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player") { 
            float randomx = Random.Range(EnemyminXPosition,EnemymaxXPosition);
            float randomy = Random.Range(-1f, 1f);
            Instantiate(enemy, new Vector3(Xposition + randomx, Yposition + randomy + ExtraPosY, 0), Quaternion.identity); //
            Destroy(gameObject);
        }
    }

    void instanceEnemy(float randomx,float randomy)
    {
        Instantiate(enemy, new Vector3(Xposition + randomx, Yposition + randomy + ExtraPosY, 0), Quaternion.identity); //

    }
}
