﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBar : MonoBehaviour {

    public Player player;
    public Animator anim;

    private int Health;

    // Use this for initialization
    void Start () {
        anim = GetComponent<Animator>();

    }
	
	// Update is called once per frame
	void Update () {
        Health = player.health; 
        anim.SetInteger("Health",Health);        
       
    }
}
