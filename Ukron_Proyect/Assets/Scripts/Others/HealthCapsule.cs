﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthCapsule : MonoBehaviour {

    public Player player;
    public Animator anim;

    public AudioSource audio;

    private int Health;
    // Use this for initialization
    void Start () {
        anim = GetComponent<Animator>();
        audio = GetComponent<AudioSource>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            audio.Play();
            player.health += 1;
            if(player.health > 5){

                player.ExtraHealth = player.health - 5;
                GameController.instance.extrahealth = player.health - 5;
            }
            Destroy(gameObject);
        }
    }
}
