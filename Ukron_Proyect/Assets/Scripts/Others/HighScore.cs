﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/**
 * Clase que controla el despliege del puntaje mas alto en la pantalla de menu.
 * 
 */
public class HighScore : MonoBehaviour
{

    public Text highscore;
    // Use this for initialization

    void Start()
    {
        highscore.text = "HighScore: " + PlayerPrefs.GetFloat("HighScore").ToString("n0");
    }

}

