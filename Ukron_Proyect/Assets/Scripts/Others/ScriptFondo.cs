﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScriptFondo : MonoBehaviour {
    //Declaramos las variables
    public float ParallaxSpeed = 0.02f;
    public RawImage background;
    public Player player;
    
	// Use this for initialization
	void Start () {

    }

    // Update is called once per frame
    void Update()
    {
        if (player.moveParallax > 0)
        {
            float finalSpeed = ParallaxSpeed * Time.deltaTime;//instanciamos a una finalSpeed
            background.uvRect = new Rect(background.uvRect.x + finalSpeed, 0f, 1f, 1f);//Movemos el fondo
        }

        if (player.moveParallax < 0)
        {
            float finalSpeed = -ParallaxSpeed * Time.deltaTime;//instanciamos a una finalSpeed
            background.uvRect = new Rect(background.uvRect.x + finalSpeed, 0f, 1f, 1f);//Movemos el fondo
        }
    }
}