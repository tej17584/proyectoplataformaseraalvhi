﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlataformaFalling : MonoBehaviour {

    public float fallDelay = 1f; //tiempo para la caida de la plataforma
    public float respawnDelay = 4f; // tiempo para que la plataforma vuelva a aparecer en donde estaba

    private Rigidbody2D rb;
    private PolygonCollider2D pc;

    private Vector3 initPosition; //sera la posicion inicial de la plataforma
	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody2D>();
        pc = GetComponent <PolygonCollider2D>();
        initPosition = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player")) { //se compara si quien esta tocando la plataforma es el jugador
            Invoke("Fall",fallDelay); // se invoca al metodo Fall() y se tarda el tiempo fallDelay en actuar
            Invoke("Respawn", fallDelay + respawnDelay); //se invoca al metodo Respawn() y se le suma el tiempo de caida
        }
    }

    void Fall() {
        rb.isKinematic = false;        
        pc.isTrigger = true;//cuando se marca que un collider es trigger entonces ya no gestiona colisiones con otros objetos
    }

    void Respawn() // metodo para que la plataforma en cuestion vuelva a su posicion inicial
    {
        transform.position = initPosition;
        rb.isKinematic = true;
        rb.velocity = Vector3.zero;
        pc.isTrigger = false;
    }
}
