﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Esta clase controla a las balas del jugador
 */
public class Bullet_Controller : MonoBehaviour {

    public float bulletSpeed = 1f;
    public float Xorientation; //orientacion en X
    public bool Facingright = true;
    private bool col = false; //variable que sirve para que la bala se detenga al momento de chocar con un enemigo

    Rigidbody2D rb2d;
    SpriteRenderer sr;
    public Animator anim;
    public CircleCollider2D cc;
    
	// Use this for initialization
	void Start () {        
        anim = GetComponent<Animator>();
        //rb2d = GetComponent<Rigidbody2D>();
        sr = GetComponent<SpriteRenderer>();
        cc = GetComponent<CircleCollider2D>();
        Xorientation = Player.Xdirection;

    }

    private void Update()
    {
        if (Xorientation != 0){
            if (col == false) { 
            //la velocidad se multiplica por un vector2, la orientacion en X(negativa o positiva), la velocidad y el tiempo.
            transform.Translate(new Vector2(1, 0) * Xorientation * bulletSpeed * Time.deltaTime); 
            Facingright = Xorientation > 0; //si la orientacion en x es mayor a 0 entonces facingRight sera true
            }
        }
        sr.flipX = !Facingright; //si facingRight es false ENTONCES sr.flipX sera verdadero y se volteara.

    }

    private void OnBecameInvisible()
    {
        DestroyObj();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Enemies") {
            col = true;
            anim.SetTrigger("Collision");
            Invoke("DestroyObj", 0.30f);
        }
        
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        
    }

    private void DestroyObj() {
        Destroy(gameObject);
    }

}
