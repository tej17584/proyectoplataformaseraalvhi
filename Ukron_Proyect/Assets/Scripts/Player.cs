﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Esta clase controla al jugador
 */
public class Player : MonoBehaviour {

    Rigidbody2D rb2d; //Hace referencia al cuerpo rigido del jugador
    SpriteRenderer sr;
    public Animator anim;
    public AudioSource audio;
    public AudioClip audioJump;
    public AudioClip audioShotL;
    public AudioClip audioShotM;
    public AudioClip audioDamage;
    public AudioClip audioDeath;

    public GameObject littleBullet; //Variables que instancian a las bala
    public GameObject mediumBullet;
    public float offsetX = 0; //Variables que posicionan la instanciacion de la bala justo en el canon de Ukron
    public float offsetY = 0;
    public float time = 0;
    public bool shooting = false;
    public bool pressing = false;
    public bool leaving = false;

    public int health = 5; //variable para la vida del jugador
    public int ExtraHealth = 0; //variable para la vida extra del juegador
    private bool death = false; 

    //variables publicas estaticas que se utilizan en otras clases 
    public static float Xdirection = 1; //esta variable sirve para tomar control de la direccion en X
    public static float positionX;
    public static float positionY;

    private float speed = 9.4f;
    public float jumpForce = 9f;
    private bool facingRight = true; //se estara viendo a la derecha por defecto
    public bool grounded = true;
    public float MaxSpeed = 5f;
    
    private bool doubleJump;//variable para salto doble
    private bool jump;

    private bool movement = true;
    public float moveParallax = 0;
    
    bool UpInput = false;
    float HInput = 0;

    // Use this for initialization
    void Start () {
        rb2d = GetComponent<Rigidbody2D>();
        sr = GetComponent<SpriteRenderer>();
        //cam.transform.position = new Vector3(rb2d.transform.position.x, cam.transform.position.y, cam.transform.position.z);
        audio = GetComponent<AudioSource>();

        anim = GetComponent<Animator>();
    }
	
	// Update is called once per frame
	void Update () {

        float move = Input.GetAxis("Horizontal"); //se toma la entrada en el eje horizontal para PC
        //float move = HInput; //entrada de los botones

        anim.SetBool("Grounded", grounded); //el valor de grounded sera el mismo del parametro de animaciones Grounded
        anim.SetBool("Death2",death); //el valor de death sera el mismo del parametro de animaciones Death2

        if (move > 0) Xdirection = 1; //se detecta la direccion en X
        if (move < 0) Xdirection = -1;
        moveParallax = move; //variable que servira para el movimiento del fondo 

        //time = time + Time.deltaTime; //Variable para llevar el tiempo en segundos 
        Debug.Log("Health: " + health.ToString("n0"));

        if (movement == false) move = 0;

        if (move != 0) {
             rb2d.transform.Translate(new Vector3(1, 0, 0) * move * speed * Time.deltaTime);
             //cam.transform.position = new Vector3(rb2d.transform.position.x, cam.transform.position.y, cam.transform.position.z);
             facingRight = move > 0; //si move es mayor a 0 entonces facingRight es true

             //anim.SetFloat("Speed", Mathf.Abs(move));
        }

        anim.SetFloat("Speed", Mathf.Abs(move));

        sr.flipX = !facingRight; //si facingRight es false ENTONCES sr.flipX sera verdadero y se volteara.

        positionX = transform.position.x; //variables para obtener las coordenadas del jugador
        positionY = transform.position.y;

        if (GameController.instance.gameover == false) { 

            if (Input.GetKeyDown(KeyCode.UpArrow)) { //PC
            //if (UpInput) {
                if (grounded == true)
                {
                    jump = true;
                    doubleJump = true;
                }
                else if (doubleJump == true)
                {
                    jump = true;
                    doubleJump = false;
                }
            }

            //Codico para la carga del disparo desde PC
            if (Input.GetKey(KeyCode.X)) {
                time = time + Time.fixedUnscaledDeltaTime;
                //Debug.Log("Time: " + time.ToString("n0"));
                if (time < 2.5)
                { //En esta condicion se crean instancias de la bala pequena en relacion a la posicion del jugador
                    if (Input.GetKeyDown(KeyCode.X) == true)
                    {
                        var player = GameObject.Find("Player"); //variable que hace referencia a player
                        if (player != null)
                        {
                            anim.SetTrigger("LittleShots"); //se llama al trigger de la animacion de disaparo pequeno  
                            Invoke("LittleShot", 0.02f);
                            //LittleShot();
                        }
                    }

                } else if (time > 2) {
                    if (Input.GetKeyDown(KeyCode.X) == false) {
                    //En esta condicion se crean instancias de la bala mediana en relacion a la posicion del jugador
                        var player = GameObject.Find("Player"); //variable que hace referencia a player
                        if (player != null)
                        {
                            anim.SetTrigger("MediumShots"); //se llama al trigger de la animacion de disaparo mediano
                            //MediumShot();
                            time = 0;
                        }

                    }
                }
            }
            //time = 0;
            //Codico para la carga del disparo desde boton
            if(pressing == true)
            {
                time = time + Time.deltaTime;
                //Debug.Log("Time: " + time.ToString("n0"));
                if (time < 2.5)
                { //En esta condicion se crean instancias de la bala pequena en relacion a la posicion del jugador
                    if (shooting == true)
                    {
                        var player = GameObject.Find("Player"); //variable que hace referencia a player
                        if (player != null)
                        {
                            anim.SetTrigger("LittleShots"); //se llama al trigger de la animacion de disaparo pequeno  
                            Invoke("LittleShot", 0.02f);
                            //LittleShot();
                        }
                        shooting = false;
                    }

                }
                else if (time > 2)
                {
                    if (leaving == true)
                    {
                        //En esta condicion se crean instancias de la bala mediana en relacion a la posicion del jugador
                        var player = GameObject.Find("Player"); //variable que hace referencia a player
                        if (player != null) {
                            anim.SetTrigger("MediumShots"); //se llama al trigger de la animacion de disaparo mediano
                                                            //MediumShot();
                            time = 0;
                        }
                        leaving = false;
                        pressing = false;

                    }
                }                
            }                       

            if (Input.GetKeyDown(KeyCode.Z)) {
                var player = GameObject.Find("Player"); //variable que hace referencia a player
                if (player != null){
                    anim.SetTrigger("MediumShots"); //se llama al trigger de la animacion de disaparo mediano
                    //MediumShot();
                }
            }

        } else { //si gameover es verdadero
            Invoke("GameOver", 2);
        }


    }

    private void FixedUpdate() {        
        //Esta funcion lo que hace es comparar entre dos valores es una funcion para poder tener un tope de velocidad
        float limitedSpeed = Mathf.Clamp(rb2d.velocity.x, -MaxSpeed, MaxSpeed);
        rb2d.velocity = new Vector2(limitedSpeed, rb2d.velocity.y);

        //PArte donde le damos una fricción simulada---------
        Vector3 fixedVelocity = rb2d.velocity;//declaramos un nuevo vector
        fixedVelocity.x *= 0.9f;//5tomamos la velocidad en x y la multiplicamos por 0.75
        if (grounded == true) {
            rb2d.velocity = fixedVelocity;
        }
        //Fin friccion simulada
        //Condicion para que salte
        if (jump == true) {
            rb2d.velocity = new Vector2(rb2d.velocity.x, 0);
            rb2d.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
            jump = false;
            audio.clip = audioJump;
            audio.loop = false;
            UpInput = false;
            audio.Play();
        }
        
    }

    public void LittleShot()
    {
        PlayAudioClip(audioShotL);
        GameObject littlebullet = Instantiate(littleBullet, transform.position, Quaternion.identity);  //se crea la bala 
        if (facingRight == true)
        {
            littlebullet.transform.position += new Vector3(offsetX, offsetY, 0); //la bala instanciada se reposiciona
        }
        else
        {
            littlebullet.transform.position += new Vector3(-offsetX, offsetY, 0); //la bala instanciada se reposiciona 
        }
    }
    public void MediumShot()
    {
        PlayAudioClip(audioShotM);
        GameObject mediumbullet = Instantiate(mediumBullet, transform.position, Quaternion.identity);
        if (facingRight == true)
        {
            mediumbullet.transform.position += new Vector3(offsetX, offsetY, 0); //la bala instanciada se reposiciona
        }
        else
        {
            mediumbullet.transform.position += new Vector3(-offsetX, offsetY, 0); //la bala instanciada se reposiciona
        }
    }

    //metodo para detecta cuando desaparecemos de la escena
    private void OnBecameInvisible()
    {
        //transform.position = new Vector3(-3, 0, 0);
        GameController.instance.gameover = true;
    }

    public void EnemyKnockBack(float enemyPosx)
    {
        if (health >= 2) {
            PlayAudioClip(audioDamage);
            health -= 1;
            if (health >= 5) {
                ExtraHealth -= 1;
                GameController.instance.extrahealth = ExtraHealth;
            }
            rb2d.velocity = new Vector2(rb2d.velocity.x, 0); //Se realizan lo mismo que en el salto solo que con una fuerza menor
            rb2d.AddForce(Vector2.up * 5f, ForceMode2D.Impulse);
            float side = Mathf.Sign(enemyPosx - transform.position.x); //se resta la posicion del jugador a la del enemigo
            rb2d.AddForce(Vector2.left * side * 2f, ForceMode2D.Impulse); //se generara una fuerza en direccion contraria al enemigo en diagonal
            movement = false;
            Invoke("EnableMovement", 1); //se vuelve a habilitar el movimiento luego de 1 segundo
            anim.SetTrigger("Damage");
        } else {
            health = health - 1;
            if (health < 1 && health > -1) { //condicion para que no se repita la animacion de muerte
                PlayAudioClip(audioDeath);
                anim.SetTrigger("Death");
                movement = false;
                death = true;
                GameController.instance.gameover = true;
            }
        }

    }

    void EnableMovement()
    {
        movement = true;
    }

    void GameOver() {
        GameController.instance.ChangeScene("TittleScreen");
    }

    // Tres metodos para el manejo del boton del disparo
    public void StartShooting(bool var) {
        shooting = var;
    }
    public void Pressing(bool var)
    {
        pressing = var;
    }
    public void Leaving(bool var)
    {
        leaving = var;
    }

    // 4 metodos para el control de los botones de movimiento
    public void Jumping()
    {
        UpInput = true;
    }
    public void StartMovingR() {
        HInput = 1f;
    }
    public void StartMovingL()
    {
        HInput = -1f;
    }
    public void StopMoving()
    {
        HInput = 0;
    }

    void PlayAudioClip(AudioClip audioclip)
    {
        audio.clip = audioclip;
        audio.loop = false;
        audio.Play();
    }

}
