﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHD_Bullet : MonoBehaviour {

    public float bulletSpeed = 1f;
    public float Xorientation; //orientacion en X
    public float Inclination = 0; //se le agrega un valor de 0 - 1 en y para la inclinacion del disparo
    private bool Facingright = false;

    Rigidbody2D rb2d;
    SpriteRenderer sr;
    public Animator anim;

    // Use this for initialization
    void Start () {
        anim = GetComponent<Animator>();
        //rb= GetComponent<Rigidbody2D>();
        sr = GetComponent<SpriteRenderer>();
      
        Xorientation = EnemyShotHD.Xdirection;
    }
	
	// Update is called once per frame
	void Update () {
        //Health = player.health;
        if (Xorientation != 0)
        {
            //la velocidad se multiplica por un vector2, la orientacion en X(negativa o positiva), la velocidad y el tiempo.
            transform.Translate(new Vector2(1, Inclination) * Xorientation * bulletSpeed * Time.deltaTime);
            Facingright = Xorientation > 0; //si la orientacion en x es mayor a 0 entonces facingRight sera true
        }
        sr.flipX = !Facingright; //si facingRight es false ENTONCES sr.flipX sera verdadero y se volteara.
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        
        if (collision.gameObject.tag == "Player")
        {
            collision.SendMessage("EnemyKnockBack", transform.position.x); //se llama al metodo EnemyKnockBack de Player

            DestroyObj();
        }
       
    }

    private void OnBecameInvisible()
    {
        DestroyObj();
    }


    private void DestroyObj()
    {
        Destroy(gameObject);
    }
}
