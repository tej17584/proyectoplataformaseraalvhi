﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckGround2 : MonoBehaviour {

    private Player EnemyRun; //referencia del jugador o personaje
                           //private Rigidbody2D rb; //referencia 

    // Use this for initialization
    void Start()
    {
        EnemyRun = GetComponentInParent<Player>();
        //rb = GetComponentInParent<Rigidbody2D>();
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Ground")
        {
            EnemyRun.grounded = true;
        }
        if (collision.gameObject.tag == "Plattform")
        {
            //cuando se colisione con una plataforma la colision se volvera el componente padre del jugador de manera 
            //que el jugador se mueva junto con la plataforma movil
            EnemyRun.transform.parent = collision.transform;
            EnemyRun.grounded = true;
        }
    }

    /**
     * Metodo que detecta cuando se termina una colision
     */
    void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Ground")
        {
            EnemyRun.grounded = false;
        }
        if (collision.gameObject.tag == "Plattform")
        {
            EnemyRun.transform.parent = null; //al dejar de tocar la plataforma el componente padre del jugador se vuelve null 
            EnemyRun.grounded = false;
        }
    }


}
