﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Esta clase controla al los enemigos que disparan horizonatal mente y diagonalmente
 */
public class EnemyShotHD : MonoBehaviour {

    private Rigidbody2D rb;
    public Animator anim;
    SpriteRenderer sr;

    public AudioSource audio;
    public AudioClip audioDestroy;
    public AudioClip audioShot;

    public GameObject EnemyBullet; //Variables que instancian a la bala
    public float offsetX = 0; //Variables que posicionan la instanciacion de la bala justo en el canon 
    public float offsetY = 0;

    public float health = 3;

    public static float Xdirection = 1; //esta variable sirve para tomar control de la direccion en X

    private bool facingRight = false; //se estara viendo a la derecha por defecto
    public bool grounded = true;

    // Use this for initialization
    void Start () {
        //rb = GetComponent<Rigidbody2D>(); //hace referencia al cuerpo rigido con el que se trabaja
        anim = GetComponent<Animator>();
        sr = GetComponent<SpriteRenderer>();
        audio = GetComponent<AudioSource>();
    }
	
	// Update is called once per frame
	void Update () {
        if (facingRight == true) Xdirection = 1; //se detecta la direccion en X
        if (facingRight == false) Xdirection = -1;

        sr.flipX = facingRight; //si facingRight es true ENTONCES sr.flipX sera verdadero y se volteara.

        Invoke("colorWhite", 0.7f);
    }

    public void BulletInstantiate() {
        PlayAudioClip(audioShot);
        GameObject bullet = Instantiate(EnemyBullet, transform.position, Quaternion.identity);
        if (facingRight == true)
        {
            bullet.transform.position += new Vector3(offsetX, offsetY, 0); //la bala instanciada se reposiciona
        }
        else
        {
            bullet.transform.position += new Vector3(-offsetX, offsetY, 0); //la bala instanciada se reposiciona 
        }
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Ground")
        {
            grounded = true;
        }
        if (collision.gameObject.tag == "Plattform")
        {
            //cuando se colisione con una plataforma la colision se volvera el componente padre del jugador de manera 
            //que el jugador se mueva junto con la plataforma movil
            transform.parent = collision.transform;
            grounded = true;
        }
    }
    /**
     * Metodo que detecta cuando se termina una colision
     */
    void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Ground")
        {
            grounded = false;
        }
        if (collision.gameObject.tag == "Plattform")
        {
            transform.parent = null; //al dejar de tocar la plataforma el componente padre del jugador se vuelve null 
            grounded = false;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision) //cuando hay una colision 
    {
        if (collision.gameObject.tag == "PlayerMshot") { //se da si se choca con el disparo mediano del player
            PlayAudioClip(audioDestroy);
            sr.color = Color.yellow;
            anim.SetTrigger("Destroy");
            Invoke("DestroyObj", 0.45f);
            GameController.instance.Score += 1250;
        }
        if (collision.gameObject.tag == "PlayerLshot") { //se da si se choca con el disparo pequeno del player
            sr.color = Color.yellow;
            health = health - 1;
            if (health == 0) {
                PlayAudioClip(audioDestroy);
                sr.color = Color.white;
                anim.SetTrigger("Destroy");
                Invoke("DestroyObj", 0.45f);
                GameController.instance.Score += 1250;
            }
        }
        if (collision.gameObject.tag == "Player") { //se da si se choca con el player  
            collision.SendMessage("EnemyKnockBack", transform.position.x); //se llama al metodo EnemyKnockBack de Player
        }
    }

    private void DestroyObj()
    {
        Destroy(gameObject);
    }

    //private void OnBecameInvisible()
    //{
    //    DestroyObj();

    //}

    void colorWhite()
    {
        sr.color = Color.white;
    }

    void PlayAudioClip(AudioClip audioclip)
    {
        audio.clip = audioclip;
        audio.loop = false;
        audio.Play();
    }

}
