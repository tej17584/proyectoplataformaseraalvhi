﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Esta clase controla al enemigo corredor
 */
public class EnemyRun : MonoBehaviour {

    public float maxSpeed = 1f;
    public float Aceleration = 1f;

    private Rigidbody2D rb;
    public Animator anim;
    SpriteRenderer sr;

    public float health = 3;

    public AudioSource audio;

    // Use this for initialization
    void Start() {
        rb = GetComponent<Rigidbody2D>(); //hace referencia al cuerpo rigido con el que se trabaja
        anim = GetComponent<Animator>();

        sr = GetComponent<SpriteRenderer>();
        audio = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update () {
        Invoke("colorWhite", 0.7f);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        rb.AddForce(Vector2.right * Aceleration);
        float limitedSpeed = Mathf.Clamp(rb.velocity.x, -maxSpeed, maxSpeed); //se pone un rango para limitar la velocidad
        rb.velocity = new Vector2(limitedSpeed, rb.velocity.y); //se le agrega el rango de velocidad al cuerpo rigido

        if (rb.velocity.x > -0.01f && rb.velocity.x < 0.01f)
        { //si la velocidad es mayor a -0.01 o menor a 0.01
            Aceleration = -Aceleration;                        //la aceleracion cambia de signo para asi ir en direccion opuesta
            rb.velocity = new Vector2(Aceleration, rb.velocity.y);
        }

        if (Aceleration < 0) //para negativos
        { //Condiciones para que el enemigo o sprite se de la vuelta dependiendo de si va a la izquiera o derecha
            transform.localScale = new Vector3(1f, 1f, 1f);
        }
        else if (Aceleration > 0)
        { //para positivos
            transform.localScale = new Vector3(-1f, 1f, 1f);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision) //cuando hay una colision 
    {
        if (collision.gameObject.tag == "PlayerMshot") { //se da si se choca con el disparo mediano del player
            audio.Play();
            anim.SetTrigger("Destroy");
            Invoke("DestroyObj", 0.45f);
            GameController.instance.Score += 1000;
        }

        if (collision.gameObject.tag == "PlayerLshot") { //se da si se choca con el disparo pequeno del player
            sr.color = Color.yellow;
            health = health - 1;
            if (health == 0) {
                audio.Play();
                sr.color = Color.white;
                anim.SetTrigger("Destroy");
                Invoke("DestroyObj", 0.45f);
                GameController.instance.Score += 1000;
            } 
        }

        if (collision.gameObject.tag == "Player") { //se da si se choca con el player  
            collision.SendMessage("EnemyKnockBack", transform.position.x); //se llama al metodo EnemyKnockBack de Player
        }
    }

    private void OnBecameInvisible() {
        DestroyObj();
    }

    private void DestroyObj()
    {
        Destroy(gameObject);

    }

    void colorWhite()
    {
        sr.color = Color.white;
    }
}

