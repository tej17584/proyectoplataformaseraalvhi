﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss_Controller : MonoBehaviour {

    private Rigidbody2D rb;
    public Animator anim;
    SpriteRenderer sr;

    public AudioSource audio;
    public AudioClip audioDestroy;
    public AudioClip audioShot;
    public AudioClip audioJump;
    public AudioClip audioMove;

    public GameObject EnemyBullet; //Variables que instancian a la bala
    public float offsetX = 0; //Variables que posicionan la instanciacion de la bala justo en el canon 
    public float offsetY = 0;

    public float health = 6;

    public static float Xdirection = 1; //esta variable sirve para tomar control de la direccion en X

    private bool facingRight = false; //se estara viendo a la derecha por defecto
    private bool destroy = false;


    public float jumpForceX = 8f;
    public float jumpForceY = 8f;
    public float maxSpeed = 1f;
    public float Aceleration = 1f;

    public bool grounded = true;
    public bool Shooting = false;
    public bool Running = false;

    public int count = 0;

    public float CicleTime = 7.3f; //el tiempo en que se cambiara de estado
    private float elapsedTime = 0f;

    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody2D>(); //hace referencia al cuerpo rigido con el que se trabaja
        anim = GetComponent<Animator>();
        sr = GetComponent<SpriteRenderer>();
        audio = GetComponent<AudioSource>();

    }

    // Update is called once per frame
    void Update()
    {
        if (facingRight == true) Xdirection = 1; //se detecta la direccion en X
        if (facingRight == false) Xdirection = -1;

        sr.flipX = facingRight; //si facingRight es true ENTONCES sr.flipX sera verdadero y se volteara.


        anim.SetBool("Running", Running);
        anim.SetBool("Shooting", Shooting);
        anim.SetBool("grounded", grounded);

        if (destroy == false) { 
            if (elapsedTime < CicleTime) {
                elapsedTime += Time.deltaTime; //se acumula el tiempo transcurrido         
            } else {
                elapsedTime = 0;
                Running = false;
                Shooting = false;
                grounded = true;
                count = 0;
            }

            if (elapsedTime > 1 && elapsedTime < 3) //condicion para que cambie a disparo en este tiempo
            {
                StartShooting();
            }
            if (elapsedTime > 3 && elapsedTime < 7) //condicion para que salte
            {
                if (count == 0)
                {
                    PlayAudioClip(audioMove);
                    count = 1;
                }
                StartMoving();
            }
            if (elapsedTime > 7 && elapsedTime < 7.3) //condicion para se mueva y ataque
            {            
                if (count == 1)
                {
                    Jumping();
                    count = 2;
                }
            }
        }

        Invoke("colorWhite", 0.7f);

        

    }

    void FixedUpdate()
    {
        if (Running == true && health > 0)
        {
            rb.AddForce(Vector2.right * Aceleration);
            float limitedSpeed = Mathf.Clamp(rb.velocity.x, -maxSpeed, maxSpeed); //se pone un rango para limitar la velocidad
            rb.velocity = new Vector2(limitedSpeed, rb.velocity.y); //se le agrega el rango de velocidad al cuerpo rigido

            if (rb.velocity.x > -0.01f && rb.velocity.x < 0.01f)
            { //si la velocidad es mayor a -0.01 o menor a 0.01
                Aceleration = -Aceleration;                        //la aceleracion cambia de signo para asi ir en direccion opuesta
                rb.velocity = new Vector2(Aceleration, rb.velocity.y);
            }

            if (Aceleration < 0) //para negativos
            { //Condiciones para que el enemigo o sprite se de la vuelta dependiendo de si va a la izquiera o derecha
                //transform.localScale = new Vector3(1f, 1f, 1f);
                facingRight = false;
            }
            else if (Aceleration > 0)
            { //para positivos
                //transform.localScale = new Vector3(-1f, 1f, 1f);
                facingRight = true;
            }
        } else if(Shooting == true){
            rb.velocity = new Vector2(0, rb.velocity.y);

        }
    }

    public void BulletInstantiate()
    {
        PlayAudioClip(audioShot);
        GameObject bullet = Instantiate(EnemyBullet, transform.position, Quaternion.identity);
        if (facingRight == true)
        {
            bullet.transform.position += new Vector3(offsetX, offsetY, 0); //la bala instanciada se reposiciona
        }
        else
        {
            bullet.transform.position += new Vector3(-offsetX, offsetY, 0); //la bala instanciada se reposiciona 
        }
    }

    private void OnTriggerEnter2D(Collider2D collision) //cuando hay una colision 
    {
        if (collision.gameObject.tag == "PlayerMshot")
        { //se da si se choca con el disparo mediano del player
            sr.color = Color.yellow;
            health = health - 3;
            if (health <= 0 && health > -10)
            {
                destroy = true;
                rb.velocity = new Vector2(0, 0);
                GameController.instance.Score += 10000;
                sr.color = Color.white;
                PlayAudioClip(audioDestroy);
                anim.SetTrigger("Destroy");
                Invoke("DestroyObj", 3f);                
                //Invoke("GameOver", 4f);
            }
        }
        if (collision.gameObject.tag == "PlayerLshot")
        { //se da si se choca con el disparo pequeno del player
            sr.color = Color.yellow;
            health = health - 1;
            if (health <= 0 && health > -10)
            {
                destroy = true;
                rb.velocity = new Vector2(0, rb.velocity.y);
                GameController.instance.Score += 10000;
                sr.color = Color.white;
                PlayAudioClip(audioDestroy);
                anim.SetTrigger("Destroy");                
                Invoke("DestroyObj", 3f);               
                //Invoke("GameOver",4f);
            }
        }
        if (collision.gameObject.tag == "Player")
        { //se da si se choca con el player  
            collision.SendMessage("EnemyKnockBack", transform.position.x); //se llama al metodo EnemyKnockBack de Player
        }
    }

    private void DestroyObj()
    {
        Destroy(gameObject);
        GameOver();
    }

    void StartShooting()
    {
        Shooting = true;
        Running = false;
        grounded = false;
    }
    void StartMoving()
    {
        Running = true;
        Shooting = false;
        grounded = false;
    }

    public void Jumping()
    {
        Running = false;
        Shooting = false;
        grounded = true;
        
        rb.velocity = new Vector2(rb.velocity.x, 0); //Se realizan lo mismo que en el salto solo que con una fuerza menor
        rb.AddForce(Vector2.up * jumpForceY, ForceMode2D.Impulse);
        if (facingRight == true) { 
            float side = Mathf.Sign(transform.position.x); //se resta la posicion del jugador a la del enemigo
            rb.AddForce(Vector2.left * side * jumpForceX, ForceMode2D.Impulse); //se generara una fuerza en direccion contraria al enemigo en diagonal
        }
        else
        {
            float side = Mathf.Sign(-transform.position.x); 
            rb.AddForce(Vector2.left * side * jumpForceX, ForceMode2D.Impulse); //se generara una fuerza en direccion contraria al enemigo en diagonal
    
        }

        PlayAudioClip(audioJump);

    }

    void colorWhite()
    {
        sr.color = Color.white;
    }

    void GameOver()
    {
        GameController.instance.ChangeScene("HighScoreScreen");
    }

    void PlayAudioClip(AudioClip audioclip)
    {
        audio.clip = audioclip;
        audio.loop = false;
        audio.Play();
    }

}
