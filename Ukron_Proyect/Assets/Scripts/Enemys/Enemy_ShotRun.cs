﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Esta clase controla al enemigo que corre y que dispara
 */
public class Enemy_ShotRun : MonoBehaviour
{

    private Rigidbody2D rb;
    public Animator anim;
    SpriteRenderer sr;

    public AudioSource audio;
    public AudioClip audioDestroy;
    public AudioClip audioShot;

    public GameObject EnemyBullet; //Variables que instancian a la bala
    public float offsetX = 0; //Variables que posicionan la instanciacion de la bala justo en el canon 
    public float offsetY = 0;

    public float health = 6;

    public static float Xdirection = 1; //esta variable sirve para tomar control de la direccion en X

    private bool facingRight = false; //se estara viendo a la derecha por defecto
    public bool grounded = true;

    public float maxSpeed = 1f;
    public float Aceleration = 1f;

    public bool Shooting = false;
    public bool Running = false;
    public bool Idle = true;

    public float CicleTime = 8f; //el tiempo en que se cambiara de estado
    private float elapsedTime = 0f;
    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody2D>(); //hace referencia al cuerpo rigido con el que se trabaja
        anim = GetComponent<Animator>();
        sr = GetComponent<SpriteRenderer>();
        audio = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (facingRight == true) Xdirection = 1; //se detecta la direccion en X
        if (facingRight == false) Xdirection = -1;

        sr.flipX = facingRight; //si facingRight es true ENTONCES sr.flipX sera verdadero y se volteara.


        anim.SetBool("Running",Running);
        anim.SetBool("Shooting", Shooting);
        anim.SetBool("Idle", Idle);
                

        if (elapsedTime < CicleTime) {

            elapsedTime += Time.deltaTime; //se acumula el tiempo transcurrido            
           
        } else {            
            elapsedTime = 0;
            Running = false;
            Shooting = false;
            Idle = true;
        }

        if (elapsedTime > 1 && elapsedTime < 2.5) //condicion para que cambie a disparo en este tiempo
        {
            StartShooting();
        }
        if (elapsedTime > 2.5 && elapsedTime < 8) //condicion para que permanesca corriendo 
        {
            StartRunning();
        }

        Invoke("colorWhite",0.7f);

    }

    void FixedUpdate()
    {
        if (Running == true && health >0) {
            rb.AddForce(Vector2.right * Aceleration);
            float limitedSpeed = Mathf.Clamp(rb.velocity.x, -maxSpeed, maxSpeed); //se pone un rango para limitar la velocidad
            rb.velocity = new Vector2(limitedSpeed, rb.velocity.y); //se le agrega el rango de velocidad al cuerpo rigido

            if (rb.velocity.x > -0.01f && rb.velocity.x < 0.01f)
            { //si la velocidad es mayor a -0.01 o menor a 0.01
                Aceleration = -Aceleration;                        //la aceleracion cambia de signo para asi ir en direccion opuesta
                rb.velocity = new Vector2(Aceleration, rb.velocity.y);
            }

            if (Aceleration < 0) //para negativos
            { //Condiciones para que el enemigo o sprite se de la vuelta dependiendo de si va a la izquiera o derecha
                //transform.localScale = new Vector3(1f, 1f, 1f);
                facingRight = false;
            }
            else if (Aceleration > 0)
            { //para positivos
                //transform.localScale = new Vector3(-1f, 1f, 1f);
                facingRight = true;
            }
        } else {          
            rb.velocity = new Vector2(0, rb.velocity.y);

        }
    }

    public void BulletInstantiate()
    {
        PlayAudioClip(audioShot);
        GameObject bullet = Instantiate(EnemyBullet, transform.position, Quaternion.identity);
        if (facingRight == true)
        {
            bullet.transform.position += new Vector3(offsetX, offsetY, 0); //la bala instanciada se reposiciona
        }
        else
        {
            bullet.transform.position += new Vector3(-offsetX, offsetY, 0); //la bala instanciada se reposiciona 
        }
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Ground")
        {
            grounded = true;
        }
        if (collision.gameObject.tag == "Plattform")
        {
            //cuando se colisione con una plataforma la colision se volvera el componente padre del jugador de manera 
            //que el jugador se mueva junto con la plataforma movil
            transform.parent = collision.transform;
            grounded = true;
        }
    }
    /**
     * Metodo que detecta cuando se termina una colision
     */
    void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Ground")
        {
            grounded = false;
        }
        if (collision.gameObject.tag == "Plattform")
        {
            transform.parent = null; //al dejar de tocar la plataforma el componente padre del jugador se vuelve null 
            grounded = false;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision) //cuando hay una colision 
    {
        if (collision.gameObject.tag == "PlayerMshot")
        { //se da si se choca con el disparo mediano del player
            sr.color = Color.yellow;
            health = health - 3;
            if (health <= 0) {
                PlayAudioClip(audioDestroy);
                sr.color = Color.white;
                anim.SetTrigger("Destroy");
                Invoke("DestroyObj", 1.2f);
                GameController.instance.Score += 1500;
            }
        }
        if (collision.gameObject.tag == "PlayerLshot")
        { //se da si se choca con el disparo pequeno del player
            sr.color = Color.yellow;
            health = health - 1;
            if (health <= 0)
            {
                PlayAudioClip(audioDestroy);
                sr.color = Color.white;                
                anim.SetTrigger("Destroy");
                Invoke("DestroyObj", 1.2f);
                GameController.instance.Score += 1500;
            }
        }
        if (collision.gameObject.tag == "Player")
        { //se da si se choca con el player  
            collision.SendMessage("EnemyKnockBack", transform.position.x); //se llama al metodo EnemyKnockBack de Player
        }
    }

    private void DestroyObj()
    {
        Destroy(gameObject);

    }

    private void OnBecameInvisible()
    {
        DestroyObj();
    }

    void StartShooting() {
        Idle = false;
        Shooting = true;
    }
    void StartRunning()
    {
        Running = true;
        Shooting = false;
    }

    void colorWhite() {
        sr.color = Color.white;
    }

    void PlayAudioClip(AudioClip audioclip)
    {
        audio.clip = audioclip;
        audio.loop = false;
        audio.Play();
    }
}

