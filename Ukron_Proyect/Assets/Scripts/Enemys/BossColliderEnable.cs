﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossColliderEnable : MonoBehaviour {

    BoxCollider2D bc;
    // Use this for initialization
    void Start () {
        bc = GetComponent<BoxCollider2D>();
    }
	
	// Update is called once per frame
	void Update () {
        if (Time.deltaTime < 1.5)
        {
            bc.enabled = false;
        } else {
            bc.enabled = true;
        }

    }
}
